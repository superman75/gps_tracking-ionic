import { Component, NgZone  } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {SplashScreen} from "@ionic-native/splash-screen";
import { BLE } from '@ionic-native/ble';
import { Geolocation } from '@ionic-native/geolocation';
import {DetailPage} from "../detail/detail";

// Bluetooth UUIDs
const BLE_SERVICE = 'ffe0';
const BLE_STATE_CHARACTERISTIC = 'ffe1';

const track_service = "ff10";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private devices: any[] = [];
  statusMessage: string;

  constructor(private navCtrl: NavController,
              private splashScreen : SplashScreen,
              private alertCtrl : AlertController,
              private ble: BLE,
              private geolocation : Geolocation,
              private ngZone: NgZone) {
    ble.enable();
  }
  ionViewDidLoad(){
    this.splashScreen.hide();

    let options = {
      enableHighAccuracy: true,
      timeout: 30000,
      maximumAge: 100
    };
    this.geolocation.getCurrentPosition(options).then(position=>{
      console.log("get permission for google map");
    });
    this.startScanning();
  }
  startScanning() {

    this.setStatus('Scanning for BLE Devices...');
    this.devices = [
      // {
      //   name : "CC2650 SensorTag",
      //   id : "5b811256-8a3c-11e8-9a94-a6cf71072f73",
      //   advertising : {},
      //   rssi : -60,
      //   services : [
      //     "GPS Trackers 1"
      //   ],
      //   charateristics : [
      //     {
      //       service : "GPS Trackers 1",
      //       characteristic : "2a00"
      //     }
      //   ]
      // },
      // {
      //   name : "CC2651 SensorTag",
      //   id : "359e2e34-8a78-11e8-9a94-a6cf71072f73",
      //   advertising : {},
      //   rssi : -60,
      //   services : [
      //   ],
      //   charateristics : [
      //   ]
      // },

    ];
    this.ble.scan( [track_service],5)
      .subscribe(device=>{
          console.log('Discovered ' + JSON.stringify(device));
          this.ngZone.run(()=>{
            this.devices.push(device);
          });
      },
        error=> {
        this.showAlert('Scan Failed', 'Error scanning for BLE devices.');
        });
    setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');

  }

  selectDevice(device: any) {
    console.log(JSON.stringify(device) + " selected");
    this.navCtrl.push(DetailPage, {
      device: device
    });
  }
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }


  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }
}
