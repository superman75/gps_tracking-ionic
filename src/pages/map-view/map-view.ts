import { Component, ElementRef, ViewChild } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, LoadingController} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";

declare let google;
/**
 * Generated class for the MapViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-view',
  templateUrl: 'map-view.html',
})
export class MapViewPage {
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private trackers : any = [];
  private markers : any = [];
  private cur_markers : any = [];
  private marker_me : any;
  private cur_latlng : any;
  private image : any = {
    url: "assets/imgs/marker.png",
    size: new google.maps.Size(25, 40),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(12.5, 40),
    scaledSize: new google.maps.Size(25, 40)
  };
  private image_me : any = {
    url: "assets/imgs/bluedot.png",
    size: new google.maps.Size(30, 30),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(15, 30),
    scaledSize: new google.maps.Size(30, 30)
  };
  constructor(private navParams: NavParams,
              private loadingCtrl : LoadingController,
              private platform : Platform,
              public geolocation : Geolocation
              ) {
    this.trackers = this.navParams.get("trackers");
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter MapPage');
    this.platform.ready().then(()=>{

      this.initMap();
      this.geolocation.watchPosition()
        .subscribe(position=>{
          this.hideMyPosition();
          this.cur_latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          if ( this.marker_me != null){
            this.marker_me.setPosition(this.cur_latlng);
          } else {
            this.marker_me = new google.maps.Marker({
              // map: this.map,
              position: this.cur_latlng,
              icon : this.image_me,
            });
          }
          this.cur_markers.push(this.marker_me);
          this.showMyPosition();
        });
    })
  }
  showMyPosition(){
    for (let i = 0; i < this.cur_markers.length; i++) {
      this.cur_markers[i].setMap(this.map);
    }
  }
  hideMyPosition(){
    for (let i = 0; i < this.cur_markers.length; i++) {
      this.cur_markers[i].setMap(null);
    }
    this.cur_markers = [];
  }

  initMap() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    let options = {
      enableHighAccuracy: true,
      timeout: 30000,
      maximumAge: 100
    };
    loading.present();
    this.geolocation.getCurrentPosition(options).then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      loading.dismiss();
      this.addMarker();
    }, (err) => {
      console.log(err);
      loading.dismiss();
    });

  }
  addMarker(){

    console.log('---------');
    this.deleteMarkers();
    for(let tracker of this.trackers){
      let latLng = new google.maps.LatLng( tracker.lat,  tracker.long);
      let marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon : this.image
      });
      this.markers.push(marker);
    }

    this.showMarkers();
  }

  setMapOnAll(map) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  showMarkers(){
    this.setMapOnAll(this.map);
  }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  deleteMarkers(){
    this.clearMarkers();
    this.markers = [];
  }
}
