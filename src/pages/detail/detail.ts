import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, AlertController} from 'ionic-angular';
import {BLE} from "@ionic-native/ble";
import {MapViewPage} from "../map-view/map-view";

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// Gps Service UUIDs
const GPS_LOCATION = 'ccc1';
@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  private statusMessage : string = "";
  private peripheral : any = {
    // name : "CC2650 SensorTag",
    // id : "5b811256-8a3c-11e8-9a94-a6cf71072f73",
    // advertising : {},
    // rssi : -60,
    // services : [
    //   "GPS Trackers 1"
    // ],
    // charateristics : [
    //   {
    //     service : "GPS Trackers 1",
    //     characteristic : "2a00"
    //   }
    // ]
  };
  private trackers : any = [];

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private ble : BLE,
              private toastCtrl : ToastController,
              private alertCtrl : AlertController,
              private ngZone : NgZone) {
    let device = navParams.get("device");
    this.setStatus('Connect to ' + device.name || device.id);

    this.ble.connect(device.id).subscribe(
      peripheral => this.OnConnected(peripheral),
      peripheral => this.OnDeviceDisconnected(peripheral)
    )

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }
  OnConnected(peripheral){
    this.ngZone.run(()=>{
      this.setStatus('');
      this.peripheral = peripheral;
    });

  }
  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  OnDeviceDisconnected(peripheral) {
    let toast = this.toastCtrl.create({
      message: 'The peripheral unexpectedly disconnected',
      duration: 3000,
      position: 'center'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      // TODO navigate back?
      // this.navCtrl.pop();
    });

    toast.present();
  }
  ionViewWillLeave() {
    console.log('ionViewWillLeave disconnecting Bluetooth');
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  openMap(charateristics :any){

    let alert1 = this.alertCtrl.create({
      title: 'Google Map',
      message: 'Do you want to see the position of trackers on Google Map?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            // TODO read and notification should use the same handler
            this.ble.startNotification( this.peripheral.id, charateristics.service, charateristics.charateristic).subscribe(
              buffer => {
                let data = new Uint8Array(buffer);
            //     let data = [1155300 , 10487169, 1154300, 10485169, 1153300, 10483169, 1152300, 10484169 ];
                let length = data.length;
                this.trackers = [];
                console.log('Received Notification: Power Switch = ' + data);
                this.ngZone.run(() => {
                  for(let i = 0;i < length/2; i++){
                    this.trackers.push({lat : data[2*i]/100000, long : data[2*i+1]/100000});
                  }
                  this.navCtrl.push(MapViewPage,{trackers : this.trackers});
                });
              }
            );
          }
        }
      ]
    });
    alert1.present();
  }
}
